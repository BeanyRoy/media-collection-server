<?php
    class User {
        private $conn;

        public $id;
        public $surname;
        public $name;
        public $email;
        public $created;

        public function __construct($db) { 
            $this->conn = $db->getConnection();
        }

        public function get($userId) {

            $stmt = file_get_contents('../sql/user/get.sql');
            $query = $this->conn->prepare($stmt);
            $query->bind_param('s', $userId);

            try {
                $query->execute();
                $query->bind_result($id, $surname, $name, $email);

                if ($query->fetch()) {
                    $this->id = $id;
                    $this->surname = $surname;
                    $this->name = $name;
                    $this->email = $email;
                    //Return mit Wrapper, der sagt, ob erfolgreich und nicht und warum ["succ" => true, "data" => $this]
                    return $this;
                }
            } catch (Exception $e) { }

            // DB disconnect

            return null; // ["succ" => false, "reason" => "resource not found"]
        }

        public function post($userId) {
            $stmt = file_get_contents('../sql/user/post.sql');
            $query = $this->conn->prepare($stmt);
            $query->bind_param('ssss', $this->surname, $this->name, $this->email, $userId);

            try {
                $query->execute();
                if ($query->affected_rows > 0) {
                    //Return mit Wrapper, der sagt, ob erfolgreich und nicht und warum
                    return $this->get($userId);
                }
            } catch (Exception $e) { }

            // DB disconnect

            return null;
        }
    }
?>