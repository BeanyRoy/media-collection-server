<?php
    header('Content-Type: application/json; charset=UTF-8');
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
    header('Access-Control-Max-Age: 3600');
    header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once '../config/database.php';
    include_once '../models/user.php';

    $db = new Database();
    $user = new User($db);
    $userId = $_REQUEST['id'];
    $result;

    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $result = $user->get($userId);

            if (isset($result)) {
                http_response_code(200);
                echo json_encode($result, JSON_PRETTY_PRINT);
            } else {
                http_response_code(404);
                echo '404 (Resource not found) - The requested resource does not exist';
            }
            return;

        case 'POST':
            $data = json_decode(file_get_contents('php://input'));

            if (!isset($data->surname) || !isset($data->name) || !isset($data->email)) {
                http_response_code(400);
                echo '400 (Bad request) - Please asure that your request body contains all necessary properties (surname, name, email)';
                return;
            }

            $user->surname = $data->surname;
            $user->name = $data->name;
            $user->email = $data->email;

            $result = $user->post($userId);
            
            if (!isset($result)) {
                http_response_code(404);
                echo '404 bei POST von USER';
            } else {
                http_response_code(200);
                echo json_encode($result, JSON_PRETTY_PRINT);
            }
            return;

        case 'PUT':
            // validation of input data
            break;

        case 'DELETE':
            break;

        default:
            http_response_code(405);
    }

     // final check if request was successful before output
    // if (isset($result)) {
    //     http_response_code(200);
    //     echo json_encode($result, JSON_PRETTY_PRINT);
    // } else {
    //     echo '404 (Resource not found) - The requested resource does not exist.';
    //     http_response_code(404);
    // }

?>