<?php
    class Database {
        private $host = 'localhost';
        private $user = 'root';
        private $pass = '';
        private $db_name = 'media_collection';

        protected $conn;

        public function __construct() {
        }

        public function getConnection() {
            $this->conn = null;

            try {
                $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db_name);
            } catch (Exception $e) {
            }

            return $this->conn;
        }
    }
?>