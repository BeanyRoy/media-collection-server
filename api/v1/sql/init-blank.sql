DROP SCHEMA IF EXISTS media_collection;
CREATE SCHEMA IF NOT EXISTS media_collection;

USE media_collection;

SET SQL_SAFE_UPDATES = 0;

-- TABLE medium-type
CREATE TABLE mc_medium_type (
	mc_mt_id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    mc_mt_name VARCHAR(50) NOT NULL
);

-- TABLE vendor
CREATE TABLE mc_vendor (
	mc_v_id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    mc_v_name VARCHAR(50) NOT NULL
);
    
-- TABLE platform
CREATE TABLE mc_platform (
	mc_p_id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    mc_p_name VARCHAR(50) NOT NULL
);
    
-- TABLE owner
CREATE TABLE mc_user (
	mc_u_id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	mc_u_surname VARCHAR(25) NOT NULL,
    mc_u_name VARCHAR(25) NOT NULL,
    mc_u_email VARCHAR(50) NOT NULL,
    mc_u_pass VARCHAR(20) NOT NULL,
    mc_u_created DATETIME NOT NULL DEFAULT now()
);

-- TABLE medium
CREATE TABLE mc_medium (
	mc_m_id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    mc_m_name VARCHAR(50) NOT NULL,
    mc_m_release DATE NOT NULL,
    mc_m_type INT(10) NOT NULL,
    mc_m_vendor INT(10) NOT NULL,
    mc_m_platform INT(10) NOT NULL,
    mc_m_owner INT(10) NOT NULL,
    
    CONSTRAINT fk_medium_to_medium_type FOREIGN KEY (mc_m_type)
    REFERENCES mc_medium_type(mc_mt_id),
    
    CONSTRAINT fk_medium_to_vendor FOREIGN KEY (mc_m_vendor)
    REFERENCES mc_vendor(mc_v_id),
    
    CONSTRAINT fk_medium_to_platform FOREIGN KEY (mc_m_platform)
    REFERENCES mc_platform(mc_p_id),
    
    CONSTRAINT fk_medium_to_user FOREIGN KEY (mc_m_owner)
    REFERENCES mc_user(mc_u_id)
);