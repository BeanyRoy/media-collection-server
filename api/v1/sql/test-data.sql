USE media_collection;

-- medium
INSERT INTO mc_medium_type (mc_mt_name)
VALUES ('Game');

-- vendor
INSERT INTO mc_vendor (mc_v_name)
VALUES ('Nintendo');

-- platform
INSERT INTO mc_platform (mc_p_name)
VALUES ('Nintendo Switch');

-- user
INSERT INTO mc_user (mc_u_surname, mc_u_name, mc_u_email, mc_u_pass)
VALUES ('Rene', 'Priem', 'priemrene@gmx.de', 'pass');

INSERT INTO mc_user (mc_u_surname, mc_u_name, mc_u_email, mc_u_pass)
VALUES ('Yasmina', 'Bouziani', 'sdfg@gmx.de', 'pass');

SET @mediumTypeId = (SELECT mc_mt_id FROM mc_medium_type LIMIT 1);
SET @vendorId = (SELECT mc_v_id FROM mc_vendor LIMIT 1);
SET @platformId = (SELECT mc_p_id FROM mc_platform LIMIT 1);
SET @ownerId = (SELECT mc_u_id FROM mc_user LIMIT 1);

INSERT INTO mc_medium (mc_m_name, mc_m_release, mc_m_type, mc_m_vendor, mc_m_platform, mc_m_owner)
VALUES ('Mario Kart 8 Deluxe', CURDATE(), @mediumTypeId, @vendorId, @platformId, @ownerId);